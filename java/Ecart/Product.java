package Ecart;

public class Product {

	private int ID;
	private int ProductCategoryID;
	private String ProductName;
	private double Cost;
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getProductCategoryID() {
		return ProductCategoryID;
	}
	public void setProductCategoryID(int productCategoryID) {
		ProductCategoryID = productCategoryID;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public double getCost() {
		return Cost;
	}
	public void setCost(double cost) {
		Cost = cost;
	}
	
	
	

}
