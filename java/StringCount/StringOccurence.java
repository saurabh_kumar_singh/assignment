package StringCount;

public class StringOccurence {
	
//This function takes a char String as input ,removes space and then prints occurence of each character.
	public static void freq(String str) {
		str=str.replace(" ", "");
		str=str.toLowerCase();
		//to Count each letter Frequency
		System.out.println("Character\tFrequency");
		for(char ch='a';ch<='z';ch++) {
			int count=0;//counter Variable
			for(int i=0;i<str.length();i++) {
				if(ch==str.charAt(i)) {
					count++;
				}
			}
			if(count!=0) {
				System.out.println(ch +" \t\t" +count);
			}
			
		}	
	}
}
